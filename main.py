

#como quiro guardar el tablero en la memoria, todavia no lo se
#voy definiendo lo que quiero hacer
from __future__ import annotations 
from typing import List, Tuple, Iterator
from textwrap import dedent 

class OutOfBounds(Exception):
    pass
class PositionTaken(Exception):
    pass

class GameState:
    def __init__(self) -> None:
        self.current = str = "0" 
        self.state : List = [None] * 9

    def copy(self) -> GameState:
        new_gs = GameState()
        new_gs.currente = self.current
        new_gs.state = self.state.copy()
        return new_gs
    
    def branches(self) -> List[ Tuple[int, int, GameState] ]:
        for i, pos in enumerate(self.state):
            if pos is None:
                new_gs = self.copy()
                new_gs.state[i] = self.current
                yield (i%3, int((i-i%3)/3), new_gs)
                


    def set(self, x:int, y:int) -> None:
        if x >= 3 or x < 0 or y < 0 or y >= 3:
            raise OutOfBounds()
        if self.state[y*3+x] is not None:
            raise PositionTaken()
        self.state[y*3+x] = self.current
        self.current = "0" if self.current == "X" else "X"
   
    def finished(self) -> bool:
        return bool(self.won()) or all(self.state)
    
    def won(self) -> str | None:
        c = self.state
        if c[0] == c[4] == c[8]:
            return c[4]
        if c[2] == c[4] == c[6]:
            return c[4]
        if c[0] == c[3] == c[6]:
            return c[0]
        if c[1] == c[4] == c[7]:
            return c[1]
        if c[2] == c[5] == c[8]:
            return c[2]
        if c[0] == c[1] == c[2]:
            return c[0]
        if c[3] == c[4] == c[5]:
            return c[3]
        if c[6] == c[7] == c[8]:
            return c[6]
        return None
        
    def __repr__(self) -> str:
        contents = [i if i is not None else " " for i in self.state]
        return dedent(f""")
        | {contents[0]} | {contents[1]} | {contents[2]}
        ---+---+---
        | {contents[3]} | {contents[4]} | {contents[5]}
        ---+---+---
        | {contents[6]} | {contents[7]} | {contents[8]}
        """)



def pedir_coordenada(c):
    while True:
        coord = input(f"Posición en {c}>")
        try:
            coord = int(coord)
            return coord
        except:
            print("Introduce un número!")
            pass

if __name__ == "__main__":
    gs = GameState()
    while not gs.finished():
        print(gs)
        while True:
            x = pedir_coordenada("X")
            y = pedir_coordenada("Y")
            try:
                gs.set(x,y)
                break
            except OutOfBounds:
                print("Estas escribiendo fuera del tablero!!")
            except PositionTaken:
                print("La posición ya está en uso")
    print(gs)
    print("Ganador " + gs.won())


            
    